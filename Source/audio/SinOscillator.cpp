//
//  SinOscillator.cpp
//  
//
//  Created by Will Fairman on 12/11/2015.
//
//

#include "SinOscillator.h"

SinOscillator::SinOscillator()
{
    setFrequency(440.f);
    phasePosition = 0.f;
    amplitude = 1.0;
}
SinOscillator::~SinOscillator()
{
    
}
float SinOscillator::getSample()
{
    const float twoPi = 2 * M_PI;
    const float phaseIncrement = (twoPi * oscFrequency) / sampleRate;
    
    phasePosition += phaseIncrement;
    
    if (phasePosition < twoPi)
    {
        phasePosition -= twoPi;
    }
    
    return sin(phasePosition) * amplitude;
    
}
void SinOscillator::setFrequency(float frequency)
{
    oscFrequency = frequency;
}
void SinOscillator::setAmplitude(int velocity)
{
    amplitude = velocity / 127;
}
void SinOscillator::setSampleRate(int rate)
{
    sampleRate = rate;
}
