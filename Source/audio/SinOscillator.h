//
//  SinOscillator.h
//  
//
//  Created by Will Fairman on 12/11/2015.
//
//

#ifndef ____SinOscillator__
#define ____SinOscillator__
#include "../../JuceLibraryCode/JuceHeader.h"
#include <stdio.h>

class SinOscillator
{
    
public:
    
    //constructor
    SinOscillator();
    
    //destructor
    ~SinOscillator();
    
    float getSample();
    void setFrequency(float frequency);
    void setAmplitude(int velocity);
    void setSampleRate(int rate);
    
private:
    float phasePosition;
    float sampleRate;
    float oscFrequency;
    float amplitude;
    
    
};

#endif /* defined(____SinOscillator__) */
