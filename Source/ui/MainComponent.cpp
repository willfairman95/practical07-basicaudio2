/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) : Thread ("CounterThread"), audio (audio_)
{
    setSize (500, 400);
    addAndMakeVisible(&gain);
    gain.addListener(this);
    gain.setRange(0.0, 1.0);
    
    startThread();
    addAndMakeVisible(&toggleButton);
    toggleButton.addListener(this);
    toggleButton.triggerClick();

}

MainComponent::~MainComponent()
{

    stopThread(500);
}

void MainComponent::resized()
{
    //gain.setBounds(getLocalBounds());
    toggleButton.setBounds(getLocalBounds());
    
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

void MainComponent::sliderValueChanged(Slider* slider)
{
    if (slider == &gain)
    {
        
    }
}

void MainComponent::run()
{
    while (threadShouldExit())
    {
        uint32 time = Time::getMillisecondCounter();
        std::cout << "Counter:" << Time::getApproximateMillisecondCounter() << "\n";
        Time::waitForMillisecondCounter (time + 100);
        
    }
    
}

void MainComponent::buttonClicked(Button* button)
{
    if (toggleButton.getToggleState())
    {
        if(!isThreadRunning())
            startThread();
    }
    else
    {
        if(isThreadRunning())
            stopThread(500);
    }
}

